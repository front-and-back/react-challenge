import React, { useState } from 'react'
import { Form, Select, Button } from  'antd';
import { AiFillCaretDown } from "react-icons/ai";

const { Option } = Select;

const UserRolesControl = ({ users, handleChange, role, addItem, clearRolesList }) => {
    const [formLayout] = useState('inline');
    return (
        <div>
            <Form layout={formLayout}>
                    <Select className="user-roles-select" onChange={ handleChange } defaultValue={ role }  suffixIcon={<AiFillCaretDown />} >
                        {
                            users.map((user) => {
                                const {id , name} = user ;
                                return(<Option value={ name }
                                key={id}>{ name }</Option>)})
                        }
                    </Select>
                    <Button className="user-roles-action" onClick={ addItem }>Add</Button>
                    <Button className="user-roles-action" onClick={ clearRolesList }>Clear</Button>
                </Form>
        </div>
    )
}

export default UserRolesControl
