import { Modal } from 'antd';

const DeleteConfirmation = ({ visible, handleOk, hideModal }) => {

    return (
        <div>
            <Modal
                title="Remove Confirmation"
                visible={visible}
                okText="Yes"
                closable={false}
                onOk={handleOk}
                onCancel={hideModal}
                wrapClassName="delete-modal"
                okType="default"
            >
                <p>Are you sure you want to remove this Security Role?</p>
            </Modal>
        </div>
    );
};

export default DeleteConfirmation
