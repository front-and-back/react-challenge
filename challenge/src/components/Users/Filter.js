import React, { useState } from 'react'
import { AiFillCaretDown } from "react-icons/ai";
import { Form, Input, Select, Button } from  'antd';

const Filter = () => {
    const [componentSize] = useState('large');
    return (
        <div>
            <Form size={componentSize} className="user-roles-filter">
                <h4>Show items with value that:</h4>
                <Form.Item>
                    <Select defaultValue="Is equal to"  suffixIcon={<AiFillCaretDown />} className="select-filter">
                        <Select.Option value="Is equal to">Is equal to</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item className="input-filter">
                    <Input />
                </Form.Item>
                <Form.Item className="filter-selector">
                    <Select defaultValue="And" suffixIcon={<AiFillCaretDown />} className="select-filter">
                        <Select.Option value="And">And</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item >
                    <Select defaultValue="Is equal to"  suffixIcon={<AiFillCaretDown />} className="select-filter">
                        <Select.Option value="Is equal to">Is equal to</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item className="input-filter" >
                    <Input />
                </Form.Item>
                <Form.Item >
                    <Button size="medium" className="filter-button" type="link">
                        Filter
                    </Button>
                    <Button size="medium" className="filter-button" type="link">
                        Clear
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}

export default Filter

