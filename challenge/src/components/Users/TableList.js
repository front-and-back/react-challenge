import React from 'react'
import { Table, Button, Skeleton } from 'antd';
import { RiDeleteBin6Line } from 'react-icons/ri';
import { RiFilter3Fill } from "react-icons/ri";
import Filter from './Filter';

const TableList = ({ items, showConfirmModal, loading }) => {
    const columns = [{
        title: 'User Role',
        dataIndex: 'userRole',
        key: 'userRole',
        className: 'user-role',
        filterIcon: <RiFilter3Fill className="filter-icon"/>,
        filterDropdown: <Filter />,
        sorter: (a, b) => a.userRole.localeCompare(b.userRole),
    },
    {
        title: '',
        dataIndex: '',
    },
    {
        title: '',
        dataIndex: 'deleteItem',
        key: 'deleteItem',
        className: 'table-icons'
    }];
    const dataSource = items.map((item, index) => ({
        key: index,
        userRole: item,
        deleteItem: <Button type="link" className="trash-icon" onClick={() => showConfirmModal(index)}><RiDeleteBin6Line /> </Button>
    }));
    return (
        <section>
            {
                loading ?
                    <Skeleton active />
                    : <Table dataSource={dataSource} columns={columns} className="user-roles" pagination={false}/>
            }
        </section>
    )
}

export default TableList
