import React, { useState, useEffect } from 'react';
import data from '../../data';
import TableList from './TableList';
import DeleteConfirmation from '../../modals/DeleteConfirmation';
import UserRolesControl from './UserRolesControl';

const UserRoles = () => {
    const [users] = useState(data);
    const [list, setList] = useState([]);
    const [role, setRole] = useState(users[0].name);
    const [loading, setLoading] = useState(true);
    const [visible, setVisible] = useState(false);
    const [index, setIndex] = useState(0);

    const getRolesList = () => {
        let listLocal = localStorage.getItem('list');
        if (listLocal) {
            setTimeout(function() {
                setList(JSON.parse(listLocal));
                setLoading(false);
            }, 1000);
        }
    }

    const handleChange = (value) => {
        setRole(value);
    }

    const addItem = () => {
        if (!role) {
            return;
        }
        setList([...list, role]);
    }

    const removeItem = () => {
        setList(list.filter((item, itemIndex) => {
            return itemIndex !== index
        }));
        setVisible(false);
    }

    const showConfirmModal = (currentIndex) => {
        setVisible(true);
        setIndex(currentIndex);
    }

    const hideConfirmModal = () => {
        setVisible(false);
    }

    const clearRolesList = () => {
        setList([]);
    }

    useEffect(() => {
        getRolesList();
    }, []);
    useEffect(() => {
        localStorage.setItem('list', JSON.stringify(list));
    }, [list]);

    return (
        <section>
            <div className="user-roles-wrapper" >
                <UserRolesControl users={ users } handleChange={ handleChange } role={ role } addItem={ addItem } clearRolesList={ clearRolesList }/>
            </div>
            <TableList items={ list } showConfirmModal={ showConfirmModal } loading={ loading } />
            <DeleteConfirmation visible={visible} handleOk={ removeItem } hideModal={ hideConfirmModal } />
        </section>
    )
}

export default UserRoles
