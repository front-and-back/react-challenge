import React from 'react'
import UserRoles from './components/Users/UserRoles'

const App = () => {
  return (
    <main>
      <UserRoles />
    </main>
  )
}

export default App
