const users = [
    {
      id: 1,
      name: 'susan smith',
    },
    {
      id: 2,
      name: 'anna johnson',
    },
    {
      id: 3,
      name: 'peter jones',
    },
    {
      id: 4,
      name: 'bill anderson',
    },
  ];
  export default users;